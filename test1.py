def sorting(s):
    if len(s) <= 1:
        return s
    elem = s[0]
    left = list(filter(lambda x: x < elem, s))
    center = [i for i in s if i == elem]
    right = list(filter(lambda x: x > elem, s))
    return sorting(left) + center + sorting(right)


print(sorting([1, 2, 6, 8, 2, -6, 3, 4]))
